This is a very simple Flightgear addon to set simulator time to some offset of real time.
It is useful when flying with other persons to keep simulator time synchronised.
