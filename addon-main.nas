# Saved setting
var config_file = nil;
var config_node = props.globals.getNode("sim/time/time-setter");
var exit_listener = nil;

var read_config = func {
    io.read_properties(config_file, config_node);
}

var write_config = func {
    io.write_properties(config_file, config_node);
}


# Set time offset
var offset_hour = props.globals.getNode("sim/time/time-setter/offset-hour", 1);
var enable = props.globals.getNode("sim/time/time-setter/enable", 1);
var wrap = props.globals.getNode("sim/time/warp", 1);

var time_set = func {
    if (!enable.getBoolValue()) return;
    wrap.setValue(offset_hour.getValue() * 3600.0);
}

var timer = maketimer(60, time_set);

var on_enable = func() {
    if (enable.getBoolValue()) {
        timer.start();
        time_set();
    } else {
        timer.stop();
    }
}

var time_listener = nil;
var enable_listener = nil;


# Addon load / unload
var main = func(addon) {
    config_file = addon.createStorageDir() ~ "/config.xml";
    read_config();

    if (exit_listener != nil) removelistener(exit_listener);
    exit_listener = setlistener("/sim/signals/exit", write_config);

    gui.menuEnable("time-setter", 1);

    if (time_listener != nil) removelistener(time_listener);
    time_listener = setlistener(offset_hour, time_set);
    if (enable_listener != nil) removelistener(enable_listener);
    enable_listener = setlistener(enable, on_enable);
    on_enable();
}

var unload = func {
    timer.stop();
    if (time_listener != nil) removelistener(listener);
    time_listener = nil;
    if (enable_listener != nil) removelistener(enable_listener);
    enable_listener = nil;

    gui.menuEnable("time-setter", 0);

    if (exit_listener != nil) removelistener(exit_listener);
    exit_listener = nil;

    write_config();
}
